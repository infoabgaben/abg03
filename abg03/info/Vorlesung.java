package abg03.info;

import java.util.ArrayList;
import java.util.List;

public class Vorlesung {

	private String name; //wie hei�t die Vorlesung
	private List<Uebungsgruppe> uebungsgruppen; //speichern der zugeh�rigen Uebungsgruppen
	
	public Vorlesung(String name) { //Konstruktor
		this.name = name;
		uebungsgruppen = new ArrayList<>(); //leere Liste initialisieren
	}
	
	public void neueUebungsgruppe(Teilnehmer... teilnehmer) throws Exception { //Uebungsgruppen hinzuf�gen
		List<Teilnehmer> teilnehmerListe = new ArrayList<>(); //Liste die uebergeben wird initialisieren
		for(Teilnehmer t : teilnehmer) //mittels Iteration alle Teilnehmer in Liste einf�gen
			teilnehmerListe.add(t);
		uebungsgruppen.add(new Uebungsgruppe(this, teilnehmerListe)); //initialisiere Uebungsgruppe und fuege in Liste ein
	}
	
	public String getName() { //getter f�r Name
		return name;
	}
	
	public List<Uebungsgruppe> getUebungsgruppen(){ //getter f�r Uebungsgruppen
		return uebungsgruppen;
	}
	
	public List<Teilnehmer> getTeilnehmerListe(){ //Funktion um alle Teilnehmer auszugeben
		List<Teilnehmer> result = new ArrayList<>();
		for(Uebungsgruppe gruppe : uebungsgruppen) { //alle Uebungsgruppen
			for(Teilnehmer t : gruppe.getTeilnehmerListe()) { //alle Teilnehmer einer Uebungsgruppe
				if(!result.contains(t)) //wenn der Teilnehmer nicht bereits in der Liste ist (weil er bspw in mehreren Gruppen ist)
					result.add(t); //hinzufuegen
			}
		}
		return result; //gefuellte Liste zurueckgeben
	}
}
