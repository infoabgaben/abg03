package abg03.info;

import java.util.ArrayList;
import java.util.List;

public class Testklasse {

	public static void main(String[] args) {

		Vorlesung vorlesung = new Vorlesung("Software Engineering"); // teste Vorlesung

		// erzeuge Arrays mit verschiedenen Anzahlen an Teilnehmern
		Teilnehmer[] teilnehmer2 = new Teilnehmer[2];
		Teilnehmer[] teilnehmer3 = new Teilnehmer[3];
		Teilnehmer[] teilnehmer15 = new Teilnehmer[15];
		Teilnehmer[] teilnehmer30 = new Teilnehmer[30];
		Teilnehmer[] teilnehmer31 = new Teilnehmer[31];

		// f�lle die Arrays
		for (int i = 0; i < teilnehmer2.length; i++)
			teilnehmer2[i] = new Teilnehmer();
		for (int i = 0; i < teilnehmer3.length; i++)
			teilnehmer3[i] = new Teilnehmer();
		for (int i = 0; i < teilnehmer15.length; i++)
			teilnehmer15[i] = new Teilnehmer();
		for (int i = 0; i < teilnehmer30.length; i++)
			teilnehmer30[i] = new Teilnehmer();
		for (int i = 0; i < teilnehmer31.length; i++)
			teilnehmer31[i] = new Teilnehmer();

		// versuche die Uebungsgruppen zu erzeugen
		erzeugeUebungsgruppe(vorlesung, teilnehmer2);
		erzeugeUebungsgruppe(vorlesung, teilnehmer3);
		erzeugeUebungsgruppe(vorlesung, teilnehmer15);
		erzeugeUebungsgruppe(vorlesung, teilnehmer30);
		erzeugeUebungsgruppe(vorlesung, teilnehmer31);

		// erzeuge eine weiter Uebungsgruppe mit Teilnehmern die in 2 Gruppen sind, um
		// vorlesung.getTeilnehmerListe() zu testen
		erzeugeUebungsgruppe(vorlesung,
				new Teilnehmer[] { teilnehmer3[2], teilnehmer15[5], teilnehmer15[9], teilnehmer30[20] });

		List<Teilnehmer> tl = vorlesung.getTeilnehmerListe(); // size sollte 48 sein (3 + 15 + 30)
		System.out.println("Weird List of Objects (which are Teilnehmer of Vorlesung) with " + tl.size() + " entries:");
		for (Teilnehmer t : tl)
			System.out.println(t);

		try {
			Uebungsgruppe gruppeOhneVorlesung = new Uebungsgruppe(asList(teilnehmer3)); // funktioneren Gruppen ohne
																						// Vorlesung?
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void erzeugeUebungsgruppe(Vorlesung vorlesung, Teilnehmer[] teilnehmer) { // Funktion die versucht
																								// die Uebungsgruppen zu
																								// erzeugen und
																								// R�ckmeldung gibt
		try {
			vorlesung.neueUebungsgruppe(teilnehmer);
			System.out.println("Uebungsgruppe mit " + teilnehmer.length + " Teilnehmer erfolgreich erzeugt!");
		} catch (Exception e) { // wenn Erzeugen gescheitert ist
			System.out.println("Uebungsgruppe mit " + teilnehmer.length + " Teilnehmern ist nicht m�glich:");
			System.out.println(e.getMessage());
		}
		System.out.println();
	}

	private static ArrayList<Teilnehmer> asList(Teilnehmer[] array) { // simple Funktion die einen Array in eine
																		// ArrayList umwandelt
		ArrayList<Teilnehmer> list = new ArrayList<>();
		for (Teilnehmer t : array)
			list.add(t);
		return list;
	}
}
