package abg03.info;

import java.util.List;

public class Uebungsgruppe {

	//Attribute
	private List<Teilnehmer> teilnehmerListe;
	private Vorlesung vorlesung;

	public Uebungsgruppe(Vorlesung vorlesung, List<Teilnehmer> teilnehmerListe) throws Exception { //Konstruktor
		this.vorlesung = vorlesung; //setze Attribut
		if (teilnehmerListe.size() < 3 || teilnehmerListe.size() > 30) //falls unzul�ssige Teilnehmeranzahl Exception schmei�en
			throw new Exception("Invalide Teilnehmer Liste! Gr��e muss zwischen 3 und 30 liegen!");
		else //sonst Attribut setzen
			this.teilnehmerListe = teilnehmerListe;
	}
	
	public Uebungsgruppe(List<Teilnehmer> teilnehmerListe) throws Exception { //Alternativer Konstruktor f�r Multiplizit�t 0 zur Vorlesung
		this(null, teilnehmerListe);
	}
	
	public Vorlesung getVorlesung() { //getter fuer Vorlesung
		return vorlesung;
	}
	
	public List<Teilnehmer> getTeilnehmerListe(){ //getter fuer Teilnehmerliste
		return teilnehmerListe;
	}

}
